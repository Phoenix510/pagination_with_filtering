﻿using Pagination;
using PaginationTests.Resources;
using System;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace PaginationTests
{
    public class PageTakingTests
    {
        [Fact]
        public void Page_four_is_taken()
        {
            uint amount = 5;
            uint pageNumber = 4;
            //uint offset = pageNumber * amount;
            var filter = new PageFilter<TestClass>(pageNumber, amount);
            var sut = new PaginationRepository<TestClass>(resources.testvalues.AsQueryable());

            var result = sut.Get(filter);

            Assert.Equal(5, result.Count());
            Assert.Equal(16, result.First().Id);
        }

        [Fact]
        public void Page_four_is_ordered_by_name()
        {
            uint amount = 5;
            uint pageNumber = 4;
            var filter = new PageFilter<TestClass>(pageNumber, amount, tc => tc.Name);
            var sut = new PaginationRepository<TestClass>(resources.testvalues.AsQueryable());

            var result = sut.Get(filter);

            Assert.Equal(5, result.Count());
            Assert.Equal(17, result.First().Id);
        }

        [Fact]
        public void Page_four_is_ordered_by_Id()
        {
            uint amount = 5;
            uint pageNumber = 4;
            var filter = new PageFilter<TestClass>(pageNumber, amount, tc => tc.Id);
            var sut = new PaginationRepository<TestClass>(resources.testvalues.AsQueryable());

            var result = sut.Get(filter);

            Assert.Equal(5, result.Count());
            Assert.Equal(16, result.First().Id);
        }

        [Fact]
        public void Page_two_of_data_with_name_started_with_a()
        {
            uint amount = 5;
            uint pageNumber = 2;
            var filter = new PageFilter<TestClass>(pageNumber, amount, filterExpressions: 
                new Expression <Func<TestClass, bool>>[] 
            { 
                t => t.Name.StartsWith('a'),
            });
            var sut = new PaginationRepository<TestClass>(resources.testvalues.AsQueryable());

            var result = sut.Get(filter);

            Assert.Equal(4, result.Count());
            Assert.Equal(29, result.First().Id);
        }

        [Fact]
        public void Page_two_of_data_with_name_started_with_a_and_ended_with_1_is_empty_cause_there_is_only_one_page_of_results()
        {
            uint amount = 5;
            uint pageNumber = 2;
            var filter = new PageFilter<TestClass>(pageNumber, amount, filterExpressions:
                new Expression<Func<TestClass, bool>>[]
            {
                t => t.Name.StartsWith('a'),
                t => t.Name.EndsWith('1'),
            });
            var sut = new PaginationRepository<TestClass>(resources.testvalues.AsQueryable());

            var result = sut.Get(filter);

            Assert.Empty(result);
        }

        [Fact]
        public void Page_one_of_data_with_name_started_with_a_and_ended_with_1_has_4_items()
        {
            uint amount = 5;
            uint pageNumber = 1;
            var filter = new PageFilter<TestClass>(pageNumber, amount, filterExpressions:
                new Expression<Func<TestClass, bool>>[]
            {
                t => t.Name.StartsWith('a'),
                t => t.Name.EndsWith('1'),
            });
            var sut = new PaginationRepository<TestClass>(resources.testvalues.AsQueryable());

            var result = sut.Get(filter);

            Assert.Equal(3, result.Count());
            Assert.Equal(30, result.First().Id);

            //ordering by id
            filter = new PageFilter<TestClass>(pageNumber, amount, t=> t.Id, true, filterExpressions:
                new Expression<Func<TestClass, bool>>[]
            {
                t => t.Name.StartsWith('a'),
                t => t.Name.EndsWith('1'),
            });

            result = sut.Get(filter);

            Assert.Equal(3, result.Count());
            Assert.Equal(32, result.First().Id);
        }

        private PageFilterResource resources = new();
    }
}
