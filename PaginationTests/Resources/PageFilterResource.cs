﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaginationTests.Resources
{
    public class TestClass
    {
        public int Id;
        public string Name;

        public TestClass(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }

    internal class PageFilterResource
    {
        public TestClass[] testvalues = new[]
        {
            new TestClass(1, "d5ftest32"),
             new TestClass(2, "d4etest31"),
              new TestClass(3, "d3dtest30"),
               new TestClass(4, "d2test29"),
                new TestClass(5, "d1test28"),
                 new TestClass(6, "c9test27"),
                  new TestClass(7, "c8test26"),
                   new TestClass(8, "c7test25"),
                    new TestClass(9, "c6test24"),
                     new TestClass(10, "c5test13"),
                      new TestClass(11, "c4test22"),
                       new TestClass(12, "c3test21"),
                        new TestClass(13, "c2test20"),
                        new TestClass(14, "c1test19"),
                      new TestClass(15, "b9test18"),
                   new TestClass(16, "b8test17"),
                   new TestClass(17, "b7test16"),
                   new TestClass(18, "b6test15"),
                   new TestClass(19, "b5test14"),
                   new TestClass(20, "b4test13"),
                   new TestClass(21, "b3test12"),
                  new TestClass(22, "b2test11"),
                  new TestClass(23, "b1test10"),
                  new TestClass(24, "a9test9"),
                  new TestClass(25, "a8test8"),
                  new TestClass(26, "a7test7"),
                  new TestClass(27, "a6test6"),
                  new TestClass(28, "a5test5"),
                  new TestClass(29, "a4test4"),
                  new TestClass(30, "a3test31"),
                  new TestClass(31, "a2test21"),
                  new TestClass(32, "a1test1"),
        };
    }
}
