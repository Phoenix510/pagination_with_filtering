﻿using Newtonsoft.Json;
using Pagination;
using PaginationTests.Resources;
using System;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace PaginationTests
{
    public class PageFilterSerializingTest
    {
        private PageFilterResource resources = new();

        [Fact]
        public void Page_filter_works_after_serialization()
        {
            uint amount = 5;
            uint pageNumber = 1;
            bool descending = true;
            var filter = new PageFilter<TestClass>(pageNumber, amount, t => t.Id, descending, filterExpressions:
                           new Expression<Func<TestClass, bool>>[]
                       {
                            t => t.Name.StartsWith('a'),
                            t => t.Name.EndsWith('1'),
                       });
            var serializedFilter = JsonConvert.SerializeObject(filter);
            var deserializedFilter = JsonConvert.DeserializeObject<PageFilter<TestClass>>(serializedFilter);
            var sut = new PaginationRepository<TestClass>(resources.testvalues.AsQueryable());

            var result = sut.Get(filter);

            Assert.Equal(3, result.Count());
            Assert.Equal(32, result.First().Id);
        }

    }
}
