﻿using Newtonsoft.Json;
using Serialize.Linq.Extensions;
using Serialize.Linq.Interfaces;
using Serialize.Linq.Nodes;
using Serialize.Linq.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Pagination
{
    public class PaginationRepository<T> where T: class
    {
        private IQueryable<T> Data;

        public PaginationRepository(IQueryable<T> data)
        {
            Data = data;
        }

        public IEnumerable<T> Get(PageFilter<T> pageFilter)
        {
            var orderByFunc = pageFilter.GetOrderByFunc();
            orderByFunc = orderByFunc is not null ?
                orderByFunc : item => true;

            var filterFunc = pageFilter.GetFilterFunc();
            if (filterFunc is null)
            {
                if(!pageFilter.Descending)
                    return Data.OrderBy(orderByFunc).Skip((int)pageFilter.Offset).Take((int)pageFilter.Amount);
                return Data.OrderByDescending(orderByFunc).Skip((int)pageFilter.Offset).Take((int)pageFilter.Amount);
            }
                                   
            if (!pageFilter.Descending)
                return Data.Where(t => filterFunc(t))
                    .OrderBy(orderByFunc)
                    .Skip((int)pageFilter.Offset)
                    .Take((int)pageFilter.Amount);
            else
                return Data.Where(t => filterFunc(t))
                    .OrderByDescending(orderByFunc)
                    .Skip((int)pageFilter.Offset)
                    .Take((int)pageFilter.Amount);
        }
    }

    public class PageFilter<T>
    {
        public uint PageNumber = 1;
        public uint Amount = 20;
        public bool Descending;
        public string FilterExpressionAsJson = null;
        public string OrderByExpressionAsJson = null;

        public PageFilter(
            uint pageNumber, uint amount,
            Expression<Func<T, object>> orderByExpression = null,
            bool descending = false,
            IEnumerable<Expression<Func<T, bool>>> filterExpressions = null)
        {
            OrderByExpressionAsJson = orderByExpression?.Body.ToJson();
            FilterExpressionAsJson = BuildExpression(filterExpressions)?.Body.ToJson() ?? null;
            PageNumber = pageNumber;
            Descending = descending;
            Amount = amount;
        }

        public Func<T, object> GetOrderByFunc()
        {
            if (OrderByExpressionAsJson is null)
                return null;

            var expressionSerializer = new ExpressionSerializer(new Serialize.Linq.Serializers.JsonSerializer());
            var orderByExpression = expressionSerializer.DeserializeText(OrderByExpressionAsJson);

            ParameterExpression param = Expression.Parameter(typeof(T), "x");
            orderByExpression = new ParameterReplacer(param).Visit(orderByExpression);
            return orderByExpression is not null ?
               Expression.Lambda<Func<T, object>>(orderByExpression, param).Compile()
                : null;
        }

        public Func<T, bool> GetFilterFunc()
        {
            if (FilterExpressionAsJson is null)
                return null;

            var expressionSerializer = new ExpressionSerializer(new Serialize.Linq.Serializers.JsonSerializer());
            var filterExpression = expressionSerializer.DeserializeText(FilterExpressionAsJson);

            ParameterExpression param = Expression.Parameter(typeof(T), "x");
            filterExpression = new ParameterReplacer(param).Visit(filterExpression);
            return Expression.Lambda<Func<T, bool>>(filterExpression, param).Compile() ??
                null;
        }

        [JsonIgnore]
        public uint Offset => (PageNumber -1) * Amount;

        private Expression<Func<T, bool>> BuildExpression(IEnumerable<Expression<Func<T, bool>>> 
            multipleExpressions)
        {
            if (multipleExpressions is null || multipleExpressions.Count() < 1)
                return null;

            ParameterExpression param = Expression.Parameter(typeof(T));

            Expression<Func<T, bool>> filterFunc = multipleExpressions.First();
            foreach (var expression in multipleExpressions.Skip(1))
            {
                var body = Expression.AndAlso(filterFunc.Body, expression.Body);
                body = (BinaryExpression)new ParameterReplacer(param).Visit(body);
                filterFunc = Expression.Lambda<Func<T, bool>>(body, param);
            }

            return filterFunc;
        }

        //https://stackoverflow.com/questions/6736505/how-to-combine-two-lambdas/6736589#6736589
        internal class ParameterReplacer : ExpressionVisitor
        {
            private readonly ParameterExpression _parameter;

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return base.VisitParameter(_parameter);
            }

            internal ParameterReplacer(ParameterExpression parameter)
            {
                _parameter = parameter;
            }
        }
    }

    public static class Base64StringExtension
    {
        public static string ToBase64(this string input)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(input));
        }

        public static string FromBase64(this string input)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(input));
        }
    }
}
